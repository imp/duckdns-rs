extern crate dotenv;
extern crate duckdns;

fn main() {
    let token = dotenv::var("DUCKDNS_TOKEN").unwrap();
    duckdns::DuckDns::new(token)
        .domains("duckdns-rs")
        .ipv4("123.45.67.89")
        .update();
}
