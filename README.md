## duckdns-rs - DuckDNS client

Usage is pretty simple. All you need is instantiate new `DuckDns` object with your token and add domains.
Then call `update()`

```rust
extern crate dotenv;
extern crate duckdns;

use duckdns::DuckDns;

fn main() {
    let token = dotenv::var("DUCKDNS_TOKEN").unwrap();
    duckdns::DuckDns::new(token).domains("mydomain").update();
}
```

In addition you may choose to specify an IP address, rather than let duckdns use the source of your request.

```rust
extern crate dotenv;
extern crate duckdns;

use duckdns::DuckDns;

fn main() {
    let token = dotenv::var("DUCKDNS_TOKEN").unwrap();
    duckdns::DuckDns::new(token).domains("mydomain").ipv4("192.168.123.234").update();
}
```


If you have `IpAddr` object you can use it directly

```rust
extern crate dotenv;
extern crate duckdns;

use duckdns::DuckDns;

fn main() {
    let token = dotenv::var("DUCKDNS_TOKEN").unwrap();
    let addr = "8.8.8.8".parse().unwrap();
    duckdns::DuckDns::new(token).domains("mydomain").ip(addr).update();
}
```

